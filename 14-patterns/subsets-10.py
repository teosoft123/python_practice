from typing import Set


def subsets(s: Set) -> Set[Set]:
    r = set()
    r.add(frozenset())
    for e in s:
        for ss in r:
            n = ss.copy()
            ts = set(n)
            ts.add(e)
            n = frozenset(ts)
        r.add(n)
    return r


print(subsets([1, 3, 5]))
