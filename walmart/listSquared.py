#  input: -6, -3, -1, 2, 4, 9,
#  output: 1 4 9 16...
#
# i.e. output is an ordered squares of input.
# input is ordered and can have non-unique numbers

# So, algorithm:
# Pass 1: over source: remember the index P of the last negative number. This is a pivot
# Pass 2: merge A[:P] and A[P:] into Out


from typing import List


def listSquared(data: List) -> List:
    if len(data) == 0:
        raise ValueError("Gotta do better than that")
    if len(data) == 1:
        return [data[0] ** 2]

    p = findPivot(data)

    out = len(data) * [None]

    o = 0
    k = p
    j = k - 1

    while j >= 0 and k < len(data):
        if data[j] < data[k]:
            out[o] = data[j]
            j -= 1
        else:
            out[o] = data[k]
            k += 1
        o += 1
    while j >= 0:
        out[o] = data[j]
        j -= 1
        o += 1
    while k < len(data):
        out[o] = data[k]
        k += 1
        o += 1

    return out


def findPivot(data):
    # Find first positive number, ie pivot p where sign change
    p = -1
    for i in range(len(data)):
        if data[i] > 0 and p < 0:
            p = i
        data[i] = data[i] ** 2
    return p


def findPivot3(data, lo, hi):
    if lo >= hi:
        return lo
    if data[(hi + lo) // 2] < 0:
        return findPivot3(data, (hi + lo) // 2 + 1, hi)
    else:
        return findPivot3(data, lo, (hi + lo) // 2)


def findPivot2(data):
    return findPivot3(data, 0, len(data) - 1)

    # Find first positive number, ie pivot p where sign change
    p = -1
    for i in range(len(data)):
        if data[i] > 0 and p < 0:
            p = i
        data[i] = data[i] ** 2
    return p


if __name__ == '__main__':
    testFindPivot = True

    if testFindPivot:
        print(findPivot2([1]))
        print(findPivot2([-1, 1]))
        print(findPivot2([-1, 0, 1]))
        print(findPivot2([-1, -1, -1]))
        print(findPivot2([-3, -1, 2, 4, 5]))
        print(findPivot2([-3, -1, 2, 4]))
        print(findPivot2([-6, -3, -1, 2, 4, 9]))
    else:
        print(listSquared([-6, -3, -1, 2, 4, 9]))
        print(listSquared([-6, -3, -1, 0, 1, 2, 2, 2, 4, 9]))
        print(listSquared([-1, 1]))
        print(listSquared([1]))
        # print(listSquared([]))
