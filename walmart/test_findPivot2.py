import pytest

from walmart.listSquared import findPivot2


@pytest.mark.parametrize("data, expected", [
    ([0], 0),
    ([1], 0),
    ([-1], 0),
    ([-1, 1], 1),
    ([-3, -2, 1, 4], 2),
    ([-1, 0, 1], 1),
    ([-1, -1, -1], 2),
    ([-3, -1, 2, 4, 5], 2),
])
def test_findPivot2(data, expected):
    assert findPivot2(data) == expected
