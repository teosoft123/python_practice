import pytest

from walmart.listSquared import findPivot2


@pytest.mark.parametrize("data, expected", [
    ([1], 0),
    ([-1, 1], 1),
    ([-1, 0, 1], 1),
    ([-1, -1, -1], 2),
    ([-3, -1, 2, 4, 5], 2),
    ([-3, -1, 2, 4], 2),
    ([-6, -3, -1, 2, 4, 9], 3),
    ([-6, -3, -1, -1, 2, 4, 9], 4),
    ([-6, -3, -2, -1, -1, 2, 2, 4, 9], 5),
])
def test_findPivot2(data, expected):
    assert findPivot2(data) == expected
