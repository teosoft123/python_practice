from abc import ABCMeta, abstractmethod


class FactorialBase(metaclass=ABCMeta):

    @abstractmethod
    def factorial(self, number):
        raise NotImplementedError
