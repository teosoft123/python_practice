'''
Created on Apr 6, 2018

@author: oleg
'''


def findFirstRepeatingChar(s):
    seen = set()
    repeating = set()
    for c in s:
        if c in seen:
            repeating.add(c)
        else:
            seen.add(c)
            
    for c in s:
        if c in repeating:
            return c

        
def findFirstRepeatingCharOnePass(s):
    MAX_INDEX = 100000000
    lowest_idx = MAX_INDEX
    m = {}
    for i, c in enumerate(s):
        if not c in m:  # haven't seen before
            m[c] = i  # first index
        else:  # seen before, smallest first index wins
            if m[c] < lowest_idx:
                lowest_idx = m[c]
                
    if lowest_idx != MAX_INDEX:
        return s[lowest_idx]
    else:
        return None


if __name__ == '__main__':
    testData = ('abbacbbcd', 'abc', 'aaaccbvnda', 'xdgfgggdgbfhhhfx', 'zybcffdcyxx')
    for string in testData:
        c = findFirstRepeatingChar(string)
        print('{} is a first repeating character'.format(c)) 
        c = findFirstRepeatingCharOnePass(string)
        print('{} is a first repeating character'.format(c)) 
    
