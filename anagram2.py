'''
Created on Apr 13, 2018

@author: oleg
'''
def isAnagram(s1, s2):
    hist = {}
    
    def populate(c):
        if not c in hist:
            hist[c] = 0
        hist[c] = hist[c] + 1
        
    def analyze(c):
        if not c in hist:
            return False # but this only returns from this function, not loop
        hist[c] = hist[c] - 1
        if hist[c] == 0:
            hist.pop(c, None)

    def loop(data, action):    
        for c in data:
            c = c.lower()
            if c.isspace():
                continue
            action(c)

    loop(s1, populate)
    loop(s2, analyze)
    

if __name__ == '__main__':
    print(isAnagram('a', 'b'))
    