'''
Created on Apr 13, 2018

@author: oleg
'''
import unittest

import anagram
import anagram2
import anagram3


testData = {
    ('123', '312', True),
    ('123', '313', False),
    ('123', '31', False),
    ('123454', '445321', True),
    ('rail safety', 'fairy tales', True),
    ('William Shakespeare', 'I am a weakish speller', True),
    ('Maße', 'ßemA', True),
    ('Madam Curie', 'Radium came', True),
    ('Quid est veritas', 'Est vir qui adest', True),
    ('Vladimir Nabokov', 'Vivian Darkbloom', True),
}

testFunctions = {
    anagram.isAnagram,
#     anagram2.isAnagram,
    anagram3.isAnagram,
}

class Test(unittest.TestCase):


    def testAnagramImplementations(self):
        for f in testFunctions:
            for t in testData:
                print('{} - {} is {} anagram'.format(t[0], t[1], 'an' if t[2] else 'not an'))
                self.assertEquals(f(t[0], t[1]), t[2])


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test']
    unittest.main()