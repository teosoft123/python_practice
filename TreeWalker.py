from collections import deque
from typing import List, Deque

from Node import Node as N


class C:
    def __init__(self):
        self.s: List = []

    def a(self, n: N, c):
        self.s.append(n)


def walk(n: N, c: C):
    if n:
        walk(n.left, c)
        walk(n.right, c)
        c.a(n, c)


def walkB(l, c):
    while len(l):
        n = l.popleft()
        if n.left:
            l.append(n.left)
        if n.right:
            l.append(n.right)
        c(n)


def walkBFS(n: N, c: C):
    l: Deque = deque()
    l.append(n)
    walkB(l, c)


if __name__ == '__main__':
    n = N(N(N(N(N(4), 5), 6), 7, N(8)), 9, N(N(10), 11, N(12, N(13))))

    #           9
    #         /   \
    #       7      11
    #      / \    /  \
    #     6   8  10   12
    #    /             \
    #   5               13
    #  /
    # 4

    walkBFS(n, lambda node: print(f'{node.data}, '))

    # c = C()
    # walk(n, c)
    # while len(c.s):
    #     n = c.s.pop()
    #     print(f'{n.data},')
