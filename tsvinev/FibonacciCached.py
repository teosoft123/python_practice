from functools import lru_cache, partial
from typing import Any


@lru_cache(maxsize=None)
def fib(n):
    if n < 2:
        return n
    return fib(n - 1) + fib(n - 2)


# if __name__ == "__main__"

def myprint(s) -> Any:
    print(s)


basetwo = partial(int, base=2)
print(basetwo('101'))

print([fib(n) for n in range(16)])
