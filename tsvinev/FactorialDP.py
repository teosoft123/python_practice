from datetime import datetime
from functools import lru_cache


def timed(func):
    def decorator(*args, **kwargs):
        start = datetime.timestamp(datetime.now())
        result = func(*args, **kwargs)
        print(f'func {func} duration: {1000 * (datetime.timestamp(datetime.now()) - start)} ms')
        return result

    return decorator


@lru_cache(maxsize=None)
def fac(n):
    if n < 2:
        return 1
    return n * fac(n - 1)


print([fac(n) for n in range(16)])


@timed
def timedFac(n: int):
    return fac(n)


print(timedFac(15))
