*** Settings ***
Documentation     Testing Factorial function
Test Template     Factorial
Library           FactorialLibrary.py

*** Test Cases ***  Number      Expected
Calculate           ${0}        ${1}
                    ${1}        ${1}
                    ${2}        ${2}
                    ${3}        ${6}
                    ${4}        ${24}
                    ${5}        ${120}
                    ${6}        ${720}
                    ${7}        ${5040}
                    ${8}        ${40320}
                    ${9}        ${362880}
                    ${10}       ${3628800}
                    ${11}       ${39916800}
                    ${12}       ${479001600}
                    ${13}       ${6227020800}
                    ${14}       ${87178291200}
                    ${15}       ${1307674368000}

*** Keywords ***
Factorial
    [Arguments]         ${number}    ${expected}
    Calculate           ${number}
    Result should be    ${expected}
