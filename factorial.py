from factorialBase import FactorialBase


class Factorial(FactorialBase):

    def factorial(self, number):

        # error handling
        if not isinstance(number, int):
            raise TypeError("Sorry. 'number' must be an integer.")
        if not number >= 0:
            raise ValueError("Sorry. 'number' must be zero or positive.")

        def inner_factorial(number):
            if number <= 1:
                return 1
            return number*inner_factorial(number-1)

        return inner_factorial(number)
