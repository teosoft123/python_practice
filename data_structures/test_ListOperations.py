from typing import List

import pytest

from data_structures.ListNode import ListNode
from data_structures.ListOperations import countNodesUpToSumLimit, printFirstN, countSumAllNodesPre, \
    countSumAllNodesPost


@pytest.mark.parametrize("data, lim, expected", [
    (ListNode.readList([1, 2, 3, 5, 10]), 1, 1),
    (ListNode.readList([1, 2, 3, 5, 10]), 4, 6),
    (ListNode.readList([1, 2, 3, 5, 10]), 3, 3),
    (ListNode.readList([1, 2, 3, 5, 10]), 11, 11),
    (ListNode.readList([1, 2, 3, 5, 10]), 25, 21)
])
def test_countNodesUpToSumLimit(data, lim, expected):
    assert countNodesUpToSumLimit(data, lim) == expected


@pytest.mark.parametrize("data, lim, expected", [
    (ListNode.readList([1, 2, 3, 7, 5, 8, 10]), 5, [1, 2, 3, 7, 5]),
    (ListNode.readList([1, 2, 3, 7, 5, 8, 10]), 50, [1, 2, 3, 7, 5, 8, 10]),
])
def test_printFirstN(data, lim, expected):
    l: List = []
    printFirstN(data, lim, lambda n, num: l.append(n.data))
    assert l == expected


@pytest.mark.parametrize("list, expectedSum", [
    (None, 0),
    (ListNode.readList([1]), 1),
    (ListNode.readList([1, 2]), 3),
    (ListNode.readList([1, 2, 3, 5, 10]), 21),
])
def test_countSumAllNodesPre(list, expectedSum):
    assert expectedSum == countSumAllNodesPre(list)


@pytest.mark.parametrize("list, expectedSum", [
    (None, 0),
    (ListNode.readList([1]), 1),
    (ListNode.readList([1, 2]), 3),
    (ListNode.readList([1, 2, 3, 5, 10]), 21),
])
def test_countSumAllNodesPost(list, expectedSum):
    assert expectedSum == countSumAllNodesPost(list)
