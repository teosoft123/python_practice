def collector(ix, bin_op):
    x = ix
    op = bin_op

    def collect(y):
        nonlocal x
        x = op(x, y)

    def result():
        return x

    return collect, result


c, r = collector(0, lambda x, y: x + y)
for i in range(5):
    c(i)

print(dir(collector))
print(f'result: {r()}')
