from data_structures.functional.functional_accumulator import collector

with collector(0, lambda x, y: x + y) as (collect, result):
    for i in range(5):
        collect(i)

