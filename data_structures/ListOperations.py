from typing import Callable

from data_structures.ListNode import ListNode


def countSumAllNodesPre(n: ListNode):
    if not n:
        return 0
    else:
        return n.data + countSumAllNodesPre(n.next)


def countSumAllNodesPost(n: ListNode):
    if not n:
        return 0
    else:
        return countSumAllNodesPre(n.next) + n.data


def countNodesUpToSumLimit(n: ListNode, lim: int) -> int:
    def helper(node, limit, sum):
        if node and sum < limit:
            return helper(node.next, limit, sum + node.data)
        else:
            return sum

    return helper(n, lim, 0)


def printFirstN(n: ListNode, num: int, pf: Callable):
    printFirstNhelper(n, num, num, pf)


def printFirstNhelper(n: ListNode, lim: int, num: int, pf: Callable):
    if n and num > 0:
        pf(n, lim - num)
        printFirstNhelper(n.next, lim, num - 1, pf)


if __name__ == "__main__":
    printFirstN(ListNode.readList([1, 2, 3, 7, 5, 8, 10]), 5,
                lambda n, num: print(f'node-{num}: data: {n.data}'))
