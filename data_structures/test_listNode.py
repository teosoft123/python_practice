import pytest

from data_structures.ListNode import ListNode as n


@pytest.mark.parametrize("data, expectedList", [
    ([1, 2, 3, 5, 10], n(1, n(2, n(3, n(5, n(10))))))
])
def test_ListNode(data, expectedList):
    constructed_list = n.readList(data)
    assert constructed_list == expectedList

    constructed_list2 = n.readList2(data)
    assert constructed_list2 == expectedList


@pytest.mark.parametrize("data, expectedList", [
    ([1, 2, 3, 5, 10], n(10, n(5, n(3, n(2, n(1))))))
])
def test_ListNodeBuildReverse(data, expectedList):
    constructed_list = n.readReverse(data)
    assert constructed_list == expectedList
