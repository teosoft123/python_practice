from __future__ import annotations

from typing import cast


class ListNode:

    def __init__(self, data: int, node: ListNode = None):
        self.data = data
        self.next = node

    def __str__(self):
        return f'n({self.data}, {self.next})'

    def __eq__(self, other):
        return self.data == other.data

    @staticmethod
    def readList(nodes: [int]) -> ListNode:
        head: ListNode = cast(ListNode, None)
        tail: ListNode = head
        for i in nodes:
            new = ListNode(i)
            if tail:
                tail.next = new
            else:
                head = new
            tail = new

        return head

    @staticmethod
    def readList2(nodes: [int]) -> ListNode:
        head: ListNode = cast(ListNode, None)
        if nodes and len(nodes) > 0:
            tail = head = ListNode(nodes[0])
            for i in nodes[1:]:
                new = ListNode(i)
                tail.next = new
                tail = new

        return head

    @staticmethod
    def readReverse(nodes: [int]) -> ListNode:
        head: ListNode = cast(ListNode, None)
        for i in nodes:
            head = ListNode(i, head)

        return head
