'''
Created on Apr 3, 2018

@author: otsvinev
'''

from test.test_itertools import isEven


def gen(start):
    for i in range(start, 0, -1):
        yield i

gen2 = (i for i in [5,4,3,2,1])

if __name__ == "__main__":

    for x in gen(5):
        print(x)

    print('---------')

    for x in gen2:
        print(x)

    print(list(gen2))

    clist = [x**2 for x in [0,1,2,3,4,5] if isEven(x)]

    cgen = (x**2 for x in [0,1,2,3,4,5] if isEven(x))

    print(clist)

    for x in cgen:
        print(x)
