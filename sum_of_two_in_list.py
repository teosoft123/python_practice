'''
Created on Apr 8, 2018

@author: oleg
'''


def sumOfTwo(ns, s):
    M = {}
    for i, n in enumerate(ns):
        if not s - n in M:
            M[s - n] = set()
        M[s - n].add(i)
                            
        if n in M:
            for k in M[n]:
                yield (k, i, ns[k], ns[i], ns[k] + ns[i])


if __name__ == '__main__':
    #--------------------0--1--2--3--4--5--6--7--8--------
    print(list(sumOfTwo([1, 2, 2, 5, 3, 7, 3, 4, 2], 5)))
    
