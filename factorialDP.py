from factorialBase import FactorialBase


class FactorialDP(FactorialBase):

    # Seed
    #    0  1 
    F = [1, 1]

    def factorial(self, number):
        if not isinstance(number, int):
            raise TypeError("Sorry. 'number' must be an integer.")
        if not number >= 0:
            raise ValueError("Sorry. 'number' must be zero or positive.")

        if number > len(self.F) - 1:  # i.e. n > greatest valid index
            # start with known
            i = len(self.F) - 1  # last known
            self.F.extend([None] * (number - i))  # make room
            while i <= number:
                self.F[i] = self.F[i-1]*i
                i += 1

        return self.F[number]
