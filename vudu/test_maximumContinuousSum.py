import pytest

from vudu.maximumContinuousSum import maximumContinuousSum


@pytest.mark.parametrize("data, expectedMaxSum, expectedLocation", [
    ([-2, 5, -1, 4, -3], 8, (1, 3)),
    ([0], 0, (0, 0)),
])
def test_maximumContinuousSum(data, expectedMaxSum, expectedLocation):
    maxSum, location = maximumContinuousSum(data)
    assert expectedMaxSum == maxSum
    assert expectedLocation == location
