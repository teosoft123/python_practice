import sys
from typing import List


# find the maximum continuous sum in a set of integers in an input array
# input: {-2,5,-1,4,-3} ; output is 8 with 5,-1,4

def maximumContinuousSum(data: List) -> int:
    overallMax = -sys.maxsize - 1
    overallLocation = (-1, -1)
    # For each element in array, calculate all continuous sums and pick the max one
    for i in range(len(data)):
        localMax = -sys.maxsize - 1
        localMaxLocation = (-1, -1)

        for j in range(i, len(data)):
            localSum = sum(data[i:j + 1])
            if localSum > localMax:
                localMax = localSum
                localMaxLocation = (i, j)

        if localMax > overallMax:
            overallMax = localMax
            overallLocation = localMaxLocation

    return overallMax, overallLocation


if __name__ == '__main__':
    print(maximumContinuousSum([-2, 5, -1, 4, -3]))
