from typing import List


def Quicksort(data: List):
    quicksort(0, len(data)-1, data)


def quicksort(low: int, hi: int, d: List):
    i = low
    j = hi
    p = d[(low + hi) // 2]
    while i <= j:
        while d[i] < p:
            i += 1
        while d[j] > p:
            j -= 1
        if i <= j:
            d[i], d[j] = d[j], d[i]
            i += 1
            j -= 1
    if low < j:
        quicksort(low, j, d)
    if i < hi:
        quicksort(i, hi, d)


if __name__ == '__main__':
    data = [1, 3, 2, 7, 1, 25, 13, 0]
    dataCopy = data.copy()
    Quicksort(data)
    print(f'{data}, {data == sorted(dataCopy)}')

