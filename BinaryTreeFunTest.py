'''
Created on Apr 4, 2018

@author: oleg
'''
import unittest

from BinaryTreeFun import walkTree
from Node import Node as mkn


tree3 = mkn(mkn(mkn(mkn(mkn(4), 5), 6), 7, mkn(8)), 9, mkn(mkn(10), 11, mkn(12, mkn(13))))


class Test(unittest.TestCase):

    def testWalkTree(self):
        actual = set()
        expected = {4, 5, 6, 7, 8, 9, 10, 11, 12, 13}
        expectedNodes = {5, 6, 7, 9, 11, 12}
        expectedLeaves = {4, 8, 10, 13}
#         TODO understand why this assert doesn't work
#         self.assertSetEqual(expected, expectedLeaves | expectedNodes, 'Test data is incorrect!')
        if expectedLeaves | expectedNodes != expected:
            self.fail('Test data is incorrect!')
        walkTree(tree3, lambda node, _: actual.add(node.data))
        self.assertSetEqual(expected, actual)
        actual.clear()
        walkTree(tree3, lambda node, isLeaf: actual.add(node.data) if isLeaf else False)
        self.assertSetEqual(expectedLeaves, actual)
        actual.clear()
        walkTree(tree3, lambda node, isLeaf: actual.add(node.data) if not isLeaf else False)
        self.assertSetEqual(expectedNodes, actual)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testMain']
    unittest.main()
