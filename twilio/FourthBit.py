fourthBitMask = 1 << 4


def fourthBit(n):
    return (n >> 3) & 1


if __name__ == '__main__':
    print(fourthBit(0b1000))
