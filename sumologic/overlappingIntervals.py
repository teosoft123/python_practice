def overlappingIntervals(data, interval):
    ordered = sorted(data, key=lambda x: x[0])
    print(ordered)
    # if start of the interval in question is smaller than
    # the end of any interval, but greater than the beginning,
    # then there's overlap
    # ? what if the original intervals overlap ?
    # looks like it^ doesn't matter

    for e in ordered:
        if e[1] >= interval[0] >= e[0]:  # le end and ge beginning
            return True

    return False


if __name__ == '__main__':
    overlappingIntervals([(1, 3), (4, 10), (20, 30)], (11, 12))
