from functools import total_ordering


@total_ordering
class Interval:  # very short for Interval
    s: int
    e: int

    def __init__(self, s, e):
        self.e = e
        self.s = s
        self._name = 'Oleg'

    @property
    def name(self):
        return f'{self._name}-{self.s}-{self.e}'

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def surname(self):
        return "Tsvinev"

    def __str__(self):
        return f's: {self.s}, e: {self.e}'

    def __lt__(self, other):
        return self.e <= other.s

    def __eq__(self, other):
        return isinstance(self, type(other)) and self.s == other.s and self.e == other.e

    def __hash__(self) -> int:
        return hash((self.s, self.e))
