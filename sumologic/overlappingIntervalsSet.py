from sumologic.Interval import Interval


def overlappingIntervalsSet(data, interval) -> bool:
    s = set(data)
    ordered = sorted(s)

    for e in ordered:
        print(e)

    # It would be interesting to implement intervals
    # that have set semantics, ie each interval is a set,
    # and some intervals are subsets to some others,
    # what about set operations?
    # say adding intervals (c.p.) would produce an interval, potentially with a gap,
    # subtracting intervals would make a smaller interval if they intersect, or empty if they don't


if __name__ == '__main__':
    interval = Interval(2, 3)
    print(f'{interval.name} {interval.surname}')
    interval.name = 'Alice'
    print(f'{interval.name} {interval.surname}')
    overlappingIntervalsSet([Interval(1, 3), Interval(1, 3), Interval(4, 10), Interval(20, 30)], (11, 12))
