import pytest

from sumologic.overlappingIntervals import overlappingIntervals


@pytest.mark.parametrize("data, interval, expected", [
    ([(1, 3), (4, 10), (20, 30)], (5, 10), True),
    ([(1, 3), (4, 10), (20, 30)], (11, 12), False),
    ([(1, 3), (4, 10), (20, 30)], (21, 22), True),
    ([(1, 3), (2, 10), (20, 30)], (2, 4), True),
    ([(1, 3), (4, 10), (12, 16), (20, 30)], (18, 18), False),
])
def test_OverlappingIntervals(data, interval, expected):
    assert overlappingIntervals(data, interval) == expected
