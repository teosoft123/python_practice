from factorialDP import FactorialDP
from factorial import Factorial


class FactorialLibrary:
    def __init__(self):
        self.fDp = FactorialDP()
        self.result = 0

    def calculate(self, number):
        self.result = self.fDp.factorial(number)

    def result_should_be(self, expected):
        if self.result != expected:
            raise AssertionError('%d != %d' % (self.result, expected))
