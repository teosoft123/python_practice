'''
Created on Apr 3, 2018

@author: otsvinev
'''
import unittest

from generators_and_comprehensions import gen


class Test(unittest.TestCase):


    def setUp(self):
        pass


    def tearDown(self):
        pass


    def testGen(self):
        x0 = gen(0)
        self.assertEqual(list(x0), [])

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testGen']
    unittest.main()
