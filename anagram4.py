'''
Created on Oct 15, 2018

@author: oleg
'''
import itertools
from itertools import zip_longest

def normalForm(s):
    return filter(lambda c: not c.isspace(), map(lambda c: c.lower(), s))

def isAnagram(s1, s2):
    print("'{}', '{}'".format(s1, s2), end=' ')
    hist = {}

    for c, c1 in zip_longest(normalForm(s1), normalForm(s2)):
        if c == None or c1 == None:
            return False
        if c == c1: 
            continue # optimization
        if not c in hist:
            hist[c] = 0
        hist[c] = hist[c] + 1
        if not c1 in hist:
            hist[c1] = 0
        hist[c1] = hist[c1] - 1
        # if key is present but value is 0, remove key
        # basically means we just saw the character we previously saw in another string
        # 1 is arbitrary default; we only care about key present and value 0
        if hist.get(c, 1) == 0:
            hist.pop(c)
        if hist.get(c1, 1) == 0:
            hist.pop(c1)

    return len(hist) == 0


if __name__ == '__main__':
    print(isAnagram('1', '1'))
    print(isAnagram('12', '123'))
    print(isAnagram('1', '2'))
    print(isAnagram('123', '123'))
    print(isAnagram('123', '312'))
    print(isAnagram('123', '313'))
    print(isAnagram('123', '31'))
    print(isAnagram('123454', '445321'))
    print(isAnagram('rail safety', 'fairy tales'))
    print(isAnagram('William Shakespeare', 'I am a weakish speller'))
    print(isAnagram('Maße', 'ßemA'))
    print(isAnagram('Madam Curie', 'Radium came'))
    print(isAnagram('Quid est veritas', 'Est vir qui adest'))
    print(isAnagram('Vladimir Nabokov', 'Vivian Darkbloom'))
