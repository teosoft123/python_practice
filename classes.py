'''
Created on Apr 3, 2018

@author: otsvinev
'''


class Dog():

    def __init__(self, name="Rex"):
        self.name = name

    def barkName(self):
        return self.name

    @classmethod
    def Chihuaua(cls):
        return cls('Chihuaua')

    @classmethod
    def Croc(cls):
        return cls()


rex = Dog("Igels")

littleDog = Dog.Chihuaua()

croc = Dog.Croc()

print(rex.barkName())

print(littleDog.barkName())

print(croc.barkName())
