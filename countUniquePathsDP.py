'''
Created on Apr 9, 2018

@author: otsvinev
'''


def printDpAsMatrix(dp):
    for r in range(len(dp)):
        for c in range(len(dp[0])):
            print('{0:3}'.format(dp[r][c]), end='')
        print()


def countUniquePaths(w, h):
    DP = [[0 for c in range(w)] for r in range(h)]
    for col in range(1, w):
        DP[0][col] = 1
    for row in range(1, h):
        DP[row][0] = 1

    if w < 2 and h < 2 and w + h < 2:  # 0,0 0,1 1,0
        return DP[h - 1][w - 1]

    for row in range(1, h):
        for col in range(1, w):
            DP[row][col] = DP[row - 1][col] + DP[row][col - 1]

    printDpAsMatrix(DP)

    return DP[h - 1][w - 1]


def printDP(w, h):
    print('w: {}, h: {}, unp: {}'.format(w, h, countUniquePaths(w, h)))


if __name__ == '__main__':
    printDP(1, 1)
    printDP(1, 2)
    printDP(2, 1)
    printDP(2, 2)
    printDP(2, 3)
    printDP(3, 2)
    printDP(3, 3)
    printDP(4, 3)
    printDP(3, 4)
    printDP(4, 4)
    printDP(5, 5)
