'''
Created on Apr 4, 2018

@author: otsvinev
'''
from Node import Node as mkn


# def mkn(left=None, data=None, right=None):
#     if left is None and right is None and not data is None:
#         return Node(None, left, None)

def printTree(n):
    if n is not None:
        if n.left is None and n.right is None:
            print(n.data)
        else:
            printTree(n.left)
            print(n.data)
            printTree(n.right)


def walkLeft(n, consumer):
    if n:
        walkLeft(n.left, consumer)
        consumer(n)


def walkRight(n, consumer):
    if n:
        consumer(n)
        walkRight(n.right, consumer)


def walkSlopes(n, consumer):
    walkLeft(n.left, consumer)
    walkRight(n.right, consumer)


def walkTree(n, consumer):
    if not n is None:
        if n.left is None and n.right is None:
            consumer(n, True)
        else:
            walkTree(n.left, consumer)
            consumer(n, False)
            walkTree(n.right, consumer)


def walkTreeIn(n, c):
    if n:
        walkTreeIn(n.left, c)
        c(n)
        walkTreeIn(n.right, c)


if __name__ == '__main__':
    n = mkn(mkn(mkn(mkn(mkn(4), 5), 6), 7, mkn(8)), 9, mkn(mkn(10), 11, mkn(12, mkn(13))))

    #     printTree(n)
    #     walkTree(n, lambda node, _: print(node.data)) # prints
    #     walkTree(n, lambda node, _: print(node.data) if node.left is None and node.right is None else False)
    # walkTree(n, lambda node, isLeaf: print('{}: {}'.format(node.data, 'leaf')) if isLeaf else print(
    #     '{}: {}'.format(node.data, 'node')))

    # def printType(node, isLeaf):
    #     nodeType = 'leaf' if isLeaf else 'node'
    #     print('{}: {}'.format(node.data, nodeType))
    #
    #
    # walkTree(n, lambda node, isLeaf: printType(node, isLeaf))

    walkTreeIn(n, lambda node: print(f'{node.data} '))

    # walkSlopes(n, lambda node: print(f'{node.data}, '))
