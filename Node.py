'''
Created on Apr 4, 2018

@author: otsvinev
'''


class Node:
    # There's only 3 cases:
    # Node(1) - here, first argument is a number, move it to data, set left to None
    # Node(left, data) and Node (data, right) - here, if first argument is a number, it's case 2, otherwise, case 1
    # Node(left, data, right) - none of arguments is None,
    left = data = right = None

    def __init__(self, *args):
        #       Node(2):
        if (len(args)) > 3 or len(args) < 1:
            raise Exception('Too many or too few arguments passed')

        if (len(args)) is 1:  # Node(1)
            self.data = args[0]
        elif (len(args)) is 2:  # Node(left, data) and Node (data, right)
            if type(args[1]) is type(0):
                self.left = args[0]
                self.data = args[1]
            else:
                self.data = args[0]
                self.right = args[1]
        else:  # Node(left, data, right)
            self.left, self.data, self.right = args[0], args[1], args[2]

        if not type(self.data) is type(0):
            raise Exception('data must be numeric type')
