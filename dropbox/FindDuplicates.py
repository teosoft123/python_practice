# input: a string path to a directory
# example: '/foo'
#
# output: a list of lists of dulpicate files in the input directory
# example:
#   [
#      ['/foo/bar.png', '/foo/images/foo.png'],
#      ['/foo/file.tmp', '/foo/other.temp', '/foo/temp/baz/that.foo']
#   ]
import hashlib
import os
from typing import Dict, List


def listFiles(directory):
    # returns all direct children of a directory
    return os.listdir(directory)


def isDir(filepath):
    # returns if a filepath is a directory
    return os.path.isdir(filepath)


def sha1Sum(inFile):
    sha1 = hashlib.sha1()
    with open(inFile, mode='rb') as f:
        while True:
            buffer = f.read(4096)
            if not buffer:
                break
            sha1.update(buffer)
    return sha1.hexdigest()


class Consumer(Dict):
    def __init__(self, keyFactory):
        self.keyFactory = keyFactory

    def accept(self, value):
        key = self.keyFactory(value)
        lst = self.get(key, None)
        if lst:
            lst.append(value)
        else:
            self[key] = [value]


def traverseFileSystem(startDir: str, consumer):
    for f in listFiles(startDir):
        fullPath = startDir + '/' + f
        if not isDir(fullPath):
            if fullPath != '.' and fullPath != '..':
                consumer.accept(fullPath)
        else:
            traverseFileSystem(fullPath, consumer)


def findDuplicates(indir) -> List[List]:
    fileSizeConsumer = Consumer(lambda f: os.stat(f).st_size)
    sha1Consumer = Consumer(sha1Sum)

    traverseFileSystem(indir, fileSizeConsumer)

    traverseFileSystem(indir, sha1Consumer)

    return [l for l in sha1Consumer.values()]


# if '__name__' == __main__:
print(findDuplicates('/Users/oleg/projects'))
