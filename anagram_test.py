
import unittest


class TestAnagram(unittest.TestCase):

    @staticmethod
    def data():
        return [
            ('1', '1', True),
            ('12', '123', False),
            ('1', '2', False),
            ('123', '123', True),
            ('123', '312', True),
            ('123', '313', False),
            ('123', '31', False),
            ('123454', '445321', True),
            ('rail safety', 'fairy tales', True),
            ('William Shakespeare', 'I am a weakish speller', True),
            ('Maße', 'ßemA', True),
            ('Madam Curie', 'Radium came', True),
            ('Quid est veritas', 'Est vir qui adest', True),
            ('Vladimir Nabokov', 'Vivian Darkbloom', True),
        ]

    def test_4(self):
        from anagram4 import isAnagram
        for s in self.data():
            test = isAnagram(s[0], s[1])
            self.assertEqual(test, s[2])
