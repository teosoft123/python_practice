def reverseBits(n):
    rev = 0

    # traversing bits of 'n' from the right
    while n > 0:

        # bitwise left shift 'rev' by 1
        rev = rev << 1

        # if current bit is '1'
        if n & 1 == 1:
            rev = rev ^ 1

        # bitwise right shift 'n' by 1
        n = n >> 1

    # required number
    return rev


def printBin(n):
    print(bin(n))


if __name__ == '__main__':
    printBin(reverseBits(0b101))
    printBin(reverseBits(0b1011101))
