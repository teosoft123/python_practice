def isPalindrome(n):
    return str(n)[::-1] == str(n)


def isBinPalindrome(n):
    return bin(n)[2:] == bin(n)[2:][::-1]


if __name__ == '__main__':
    print(bin(0x11011))
    print(isPalindrome(123))
    print(isPalindrome(123321))
    print(isPalindrome(123456787654321))
    print(isBinPalindrome(0x11011))
    print(isBinPalindrome(0b110000010000011))
    print(isBinPalindrome(0b010000010000010))
