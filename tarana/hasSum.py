# Find all combination of numbers amounting to
# a given sum
# IE given [3,2,4,1] and 4 return [0,3] and [2]

from typing import List


def binToIdx(n: int, nBit: int) -> List:
    out = []
    for p in range(nBit):
        if n & (1 << p):
            out.append(p)
    return out


def hasSum(data: List, sum: int, out: List) -> None:
    nBit = len(data)
    c = (1 << nBit)
    for i in range(1, c):
        s = 0
        for idx in binToIdx(i, nBit):
            s = s + data[idx]
        if s == sum:
            out.append(binToIdx(i, nBit))


if __name__ == '__main__':
    out = []
    hasSum([1, 2, 3], 3, out)
    print(out)
