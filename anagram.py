'''
Created on Apr 13, 2018

@author: oleg
'''

def isAnagram(s1, s2):
    hist = {}
    
    for c in s1:
        c = c.lower()
        if c.isspace():
            continue
        if not c in hist:
            hist[c] = 0
        hist[c] = hist[c] + 1

    for c in s2:
        c = c.lower()
        if c.isspace():
            continue
        if not c in hist:
            return False
        hist[c] = hist[c] - 1
        if hist[c] == 0:
            hist.pop(c, None)
            
    return len(hist) == 0

if __name__ == '__main__':
    print(isAnagram('123', '312'))
    print(isAnagram('123', '313'))
    print(isAnagram('123', '31'))
    print(isAnagram('123454', '445321'))
    print(isAnagram('rail safety', 'fairy tales'))
    print(isAnagram('William Shakespeare', 'I am a weakish speller'))
    print(isAnagram('Maße', 'ßemA'))
    print(isAnagram('Madam Curie', 'Radium came'))
    print(isAnagram('Quid est veritas', 'Est vir qui adest'))
    print(isAnagram('Vladimir Nabokov', 'Vivian Darkbloom'))
    
